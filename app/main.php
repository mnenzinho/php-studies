<?php
$loader = require __DIR__ . '/../vendor/autoload.php';



use App\Core\Foo;
use \Util\Printer\Bar;

$foo = new Foo;

var_dump($foo);

$bar = new Bar();

var_dump($bar);

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$level = Logger::INFO; // | Logger::NOTICE  | logger::WARNING
$log = new Logger('name');
$logHanndler = new StreamHandler( APP['path'] . '/logs/app.log', $level);
$log->pushHandler($logHanndler);

// add records to the log
$log->warning("foo warn");
$log->error("bar error");
$log->notice("send log");

var_dump(APP, $logHanndler, $level );