<?php
/**
 *  App config
 */
define("APP", [
    'version' => '1.0.0',
    'locale' => "pt_BR",
    'database' => [
        'driver' => 'mysql',
        'port' => '3306',
        'host' => 'localhost',
        'database' => 'db',
        'password', ''
    ]
]);

/**
 * Social Network config
 */
define('SOCIAL_NETWORK', [
    'facebook' => ['user' => 'userA'],
    'google' => ['user' => 'userA'],
    'twitter' => ['user' => 'userA']
]);

echo "load config file\n";